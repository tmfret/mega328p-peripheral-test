#ifndef COMMON_H_INCLUDED
#define COMMON_H_INCLUDED

//These allow easier bit operation
#define SETBIT(ADDRESS,BIT) (ADDRESS |= (1<<BIT)) //SETBIT(PORTX, PIN)
#define CLEARBIT(ADDRESS,BIT) (ADDRESS &= ~(1<<BIT)) //CLEARBIT(PORTX, PIN)
#define FLIPBIT(ADDRESS,BIT) (ADDRESS ^= (1<<BIT)) //FLIPBIT(PORTX, PIN)
#define CHECKBIT(ADDRESS,BIT) (ADDRESS & (1<<BIT)) //CHECKBIT(PORTX, PIN)

#define SETBITMASK(x,y) (x |= (y))
#define CLEARBITMASK(x,y) (x &= (~y))
#define FLIPBITMASK(x,y) (x ^= (y))
#define CHECKBITMASK(x,y) (x & (y))

#define BIT(x) (1 << (x))
#define SETBITS(x,y) ((x) |= (y))
#define CLEARBITS(x,y) ((x) &= (~(y)))
#define BITSET(x,y) ((x) & (BIT(y)))
#define BITCLEAR(x,y) !BITSET((x), (y))
#define BITSSET(x,y) (((x) & (y)) == (y))
#define BITSCLEAR(x,y) (((x) & (y)) == 0)
#define BITVAL(x,y) (((x)>>(y)) & 1)

//#define F(string_literal) (reinterpret_cast<const __FlashStringHelper *>(PSTR(string_literal)))

    //Debug Macro. Use 'PRINTD ("debug message");' to output debugging information
    #ifdef DEBUG
        #define PRINTD printf
    #else
        #define PRINTD(format, args...) ((void)0)
    #endif
#endif // COMMON_H_INCLUDED
