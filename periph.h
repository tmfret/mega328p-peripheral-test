#ifndef PERIPH_H_INCLUDED
#define PERIPH_H_INCLUDED

/// Pin Defines
#define PWM_LED_PIN     PB3
#define LED_PIN         PB5
#define US_TRIGGER_PIN  PD3
#define US_ECHO_PIN     PD2
#define ACC_X_PIN       PC0
#define ACC_Y_PIN       PC1
#define ACC_Z_PIN       PC2
#define PRS_PIN         PC3

/// ADC Defines
#define ADMUX_MSK       0x40                // Ref set to avcc
#define ADC_33V_MAX     676                 // Max counts for a 3.3v signal on 5v aref
#define NUM_SMPL        50

/// LCD Defines
#define LCD_NULL_STR    "XXX"

#define LCD_TEMP        5,0 // Poition to print the temperature
#define LCD_PRS         5,1
#define LCD_US          5,2
#define LCD_ACC_X       15,0
#define LCD_ACC_Y       15,1
#define LCD_ACC_Z       15,2

/// HC-SR04 Defines
#define US_IN_MOD       148 // Modifier per the datasheet
#define US_CM_MOD       58 // Modifier per the datasheet

/// Timer Defines
#define t_period_1      1/F_CPU
#define t_period_1024   1024/F_CPU

#define t_1_clocks_uS   0.000001/t_period_1
#define t_1024_clocks_uS 0.000001/t_period_1024

#define t_1_clocks_S    1/t_period_1
#define t_1024_clocks_S 1/t_period_1024

#define t0_max_clocks   255
#define t1_max_clocks   65536

#define timer_up        1
#define timer_down      0

/// Accelerometer Defines
#define acc_x_adc       0
#define acc_y_adc       1
#define acc_z_adc       2

#define acc_scale       63           // Number of counts to read 1g
#define acc_0_g         385          // Counts at 0g

/// DS18S20 Defines
#define DEG             0xDF
#define C               0x43
#define F               0x46

/// Pressure Sensor Defines
#define prs_adc                 3
#define prs_oh_psi              14.88197
#define prs_oh_avg_counts       924         // Average pressure for ohio is 14.88197psi. Measured counts at this pressure
#define prs_oh_step_psi         prs_oh_psi/prs_oh_avg_counts // psi per count
#define PRS_CONV(prs)           prs*prs_oh_step_psi

/// Definitions
//Change if this struct name changes (main.c, periph.c)
struct adxl_g_values
{
    uint16_t x, y, z;
};

/// Proto
void int0_init( void );

void t0_init( void );
uint32_t t1_totalTimeUs( void );
void t1_init( void );
void t2_pwm_init( void );
void t2_setPWMCoeff( uint8_t num );

void adc_init( void );

void us_init( void );
uint8_t hcsr04_isSensing( void );
void hcsr04_resetSensingComp( void );
uint8_t hcsr04_getDist( uint8_t incm);

uint8_t ds18b20_dataReady( void );
uint16_t ds18b20_print( uint8_t degree );
void ds18b20_resetStatus( void );
void ds18b20_getTemp( void );
uint16_t ds18b20_printTemp( uint8_t deg );

uint8_t adxl_dataReady( void );
void adxl_resetStatus( void );
struct adxl_g_values adxl_getADXLGValues ( void );
struct adxl_g_values averageADXLSamples(struct adxl_g_values *sample_array);

uint8_t mpx_dataReady( void );
void mpx_resetStatus( void );
double mpx_getValue(void);
double averageMPXSamples(uint16_t *sample_array);

uint8_t lcd_ready( void );
void lcd_resetStatus( void );
void lcd_textInit( void );
void lcd_updateVals( uint16_t temp_val, double acc_x_val, double acc_y_val, double acc_z_val, uint8_t distance_val, double prs_val );

#endif // PERIPH_H_INCLUDED
