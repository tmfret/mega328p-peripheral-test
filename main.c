/*
 */

#include <stdio.h>
#include <stdint.h>				// definitions such as 'uint8_t', etc.
#include <avr/io.h>             // standard avr defs
#include <util/delay.h>
#include <stdlib.h>
#include <avr/interrupt.h>    // Needed to use interrupts
//#include <avr/pgmspace.h>
//#include <string.h>

#include "main.h"
#include "common.h"             // Easier BIT manipulation
#include "periph.h"
#include "lib/avr-uart/uart.h"
#include "lib/ds18b20.h"
#include "lib/lcdpcf8574.h"

uint8_t dist_in;
uint8_t dist_cm;
double temp_conv;
double adxl_x, adxl_y, adxl_z;
double mpx_val;

extern struct adxl_g_values; // Defined in periph.h

int main(void)
{

    //stdout = &mystdout; // Set up printf

    DDRB |= (1 << LED_PIN); // Set up the LED pin as an output
    DDRB |= (1 << PWM_LED_PIN); // PB3 as output (PWM)

    us_init();
    int0_init();
    t0_init();
    t1_init();
    t2_pwm_init();
    adc_init();

    sei(); // Enable the Global Interrupt Enable flag so that interrupts can be processed

    lcd_textInit();

    initUART();
    writeString("Init Complete\n");

    while(1)
    {
        // Send the initial pulse to the ultrasonic sensor (10us)
        if (!hcsr04_isSensing())
        {
            SETBIT(PORTD, US_TRIGGER_PIN);
            _delay_us(12);
            CLEARBIT(PORTD, US_TRIGGER_PIN);
        }

        else if (hcsr04_isSensing())
        {

            _delay_ms(60); // Delay to debounce the US
            hcsr04_resetSensingComp();
            dist_in = hcsr04_getDist( 0 );
            dist_cm = hcsr04_getDist( 1 );

            // Vary PWM speed based on distance from sensor
            if (dist_in <= 18)
            {
                t2_setPWMCoeff(4);
            }
            else if (dist_in <= 50)
            {
                t2_setPWMCoeff(3);
            }
            else if (dist_in <= 100)
            {
                t2_setPWMCoeff(2);
            }
            else if (dist_in >= 100)
            {
                t2_setPWMCoeff(1);
            }
        }

        if (ds18b20_dataReady())
        {
            temp_conv = ds18b20_print(F);
            ds18b20_resetStatus();
        }

        // Print all three values if they have been converted
        if (adxl_dataReady())
        {
            //writeString(acc_x);
            //acc_x = averageSamples(adc_acc_x_samples);

            struct adxl_g_values adxl_g_values = adxl_getADXLGValues();
            adxl_x = adxl_g_values.x;
            adxl_y = adxl_g_values.y;
            adxl_z = adxl_g_values.z;

            //writeString("\n");
            adxl_resetStatus();
        }

        // Print the raw pressure value
        if (mpx_dataReady())
        {
            //TODO: printf("PSI:%u\n", prs_val);
            mpx_val = mpx_getValue();
            mpx_resetStatus();
        }

        if (lcd_ready())
        {
            lcd_updateVals(temp_conv, adxl_x, adxl_y, adxl_z, dist_in, mpx_val);
            lcd_resetStatus();
        }
    }
    return 0;
}
