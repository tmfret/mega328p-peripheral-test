#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

/// Global Defines
#define VCC             5 //Current nominal voltage for the mcu

/// Function Declarations
int main( void );

#endif // MAIN_H_INCLUDED
