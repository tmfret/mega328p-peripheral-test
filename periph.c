#include <stdio.h>
#include <stdint.h>				// definitions such as 'uint8_t', etc.
#include <avr/io.h>             // standard avr defs
#include <util/delay.h>
#include <stdlib.h>
#include <avr/interrupt.h>    // Needed to use interrupts
#include "common.h"
#include "periph.h"
#include "lib/avr-uart/uart.h"
#include "lib/ds18b20.h"
#include "lib/lcdpcf8574.h"

/// ADC Vars
volatile uint8_t adc_sample; // The number of samples taken
volatile uint8_t first_read; // Var to sthrow out the first ADC read
volatile uint16_t adc_raw;
volatile struct adxl_g_values adc_adxl_samples[NUM_SMPL], *adxl_g_ptr; // Pointer to last struct in array
volatile uint16_t adc_mpx_samples[NUM_SMPL], *mpx_val_ptr;

/// HC-SR04 Vars
volatile uint8_t sensing; // Detect whether we're sensing or not
volatile uint8_t sensing_complete; // Detect Sensing complete
volatile uint8_t distance_in; // Distance to target in in
volatile uint8_t distance_cm; // Distance to target in cm
uint8_t distance_old;

/// Timer0 Vars
volatile uint16_t t0_ovf_count;

/// Timer1 Vars
volatile uint16_t t1_ovf_count; // The number of timer overflows to calculate
volatile uint16_t t1_stop_val; // The timer stop value
volatile uint32_t t1_total_time_uS; // Total time elapsed in uS

/// Timer2 Vars
uint8_t t2_dir = timer_up; // Direction to count for PWM
volatile uint8_t t2_dist_coef; // Number of counts to add to the PWM per overlow

/// DS18S20 Temp Sensor Vars
uint8_t update_temp;
uint16_t raw_temperature, raw_temperature_old;
volatile uint8_t degree = 0x00; // C or F

/// Accelerometer Vars
uint8_t adc_ch = acc_x_adc; // Default to first ADC channel
double acc_x, acc_x_old;
double acc_y, acc_y_old;
double acc_z, acc_z_old;
volatile uint8_t acc_complete = 0; // Tells main() when to print the accelerometer values

double acc_conv_d; // Output double
uint8_t acc_conv_out_x[5]; // Output for dtostrf (+1 for null char)
uint8_t acc_conv_out_y[5]; // Output for dtostrf (+1 for null char)
uint8_t acc_conv_out_z[5]; // Output for dtostrf (+1 for null char)

/// Pressure Sensor Vars
double prs_val; // ADC pressure sensor value
double prs_val_old;
volatile uint8_t prs_complete; // Tells main() when the value has been read
double prs_conv_d; // Output double
uint8_t prs_conv_out[6] ; // Output for dtostrf (+1 for null char)

/// LCD Vars
volatile uint8_t update_lcd = 0;
volatile uint8_t lcd_done = 1;

void int0_init(void)
{
    //Setup INT0
    EICRA |= (1 << ISC00); // INT0 interrupt on any logical change
    EIMSK |= (1 << INT0); // Enable INTO interrupt bit
}

void us_init(void)
{
    //Setup the ultrasonic sensor pins
    DDRD &= ~(1 << US_ECHO_PIN); // Set as input
    PORTD |= (1 << US_ECHO_PIN); // Turn on pullup resistor
    DDRD |= (1 << US_TRIGGER_PIN); // Configure the trigger pin as an output
}

void t0_init(void)
{
    TCCR0B |= (1 << CS00) | (1 << CS02); // Set up timer 0 (prescale 1024)
    TIMSK0 |= (1 << TOIE1); // Turn on timer0 overflow interrupt
}

void t1_init(void)
{
    TIMSK1 |= (1 << TOIE1); // Turn on the timer1 overflow interrupt
}

uint32_t t1_totalTimeUs ( void )
{
    uint32_t time = t1_total_time_uS = ((t1_ovf_count * t1_max_clocks) + t1_stop_val) / t_1_clocks_uS;
    return (time);
}

void t2_pwm_init(void)
{
    OCR2A = 0; // Set PWM for 0% duty cycle to start
    TCCR2A |= (1 << COM2A1); // Set non-inverting mode
    TCCR2A |= (1 << WGM21) | (1 << WGM20); // Set fast PWM Mode
    TCCR2B |= (1 << CS21) | (1 << CS22); // Set prescaler to 1024 and starts PWM
    TIMSK2 |= (1 << OCIE2A); // Enable CTC interrupt
}

void t2_setPWMCoeff (uint8_t num)
{
    if (num <= 4)
    {
        t2_dist_coef = num;
    }
}

void adc_init(void)
{
    ADMUX |= (1 << REFS0); // Use AVCC as voltage ref
    //ADMUX |= (1 << ADLAR); // Left adjust ADC result to allow easy 8 bit reading
    ADMUX |= (1 << ACC_X_PIN); // Clear MUX0:4 and set MUX0 to 1
    ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0); // Set ADC prescaler to 128 - 125KHz sample rate @ 16MHz
    ADCSRA |= (1 << ADATE); // Set trigger on positive edge of signal
    ADCSRA |= (1 << ADEN); // Enable the ADC
    ADCSRA |= (1 << ADIE); // Enable interrupt
    ADCSRA |= (1 << ADSC);  // Start A2D Conversions
}

void lcd_textInit()
{

    lcd_done = 0;

    lcd_init(LCD_DISP_ON);
    lcd_clrscr();

    lcd_printf("Init Complete");
    lcd_led(1);
    _delay_ms(500);

    lcd_led(0);
    _delay_ms(500);

    lcd_clrscr();
    lcd_gotoxy(0,0);

    lcd_clrscr();
    lcd_gotoxy(0,0);
    lcd_printf("Temp:");
    lcd_gotoxy(0,1);
    lcd_printf("Pres:");
    lcd_gotoxy(0,2);
    lcd_printf("Dist:");

    lcd_gotoxy(10,0);
    lcd_printf("AccX:");
    lcd_gotoxy(13,1);
    lcd_printf("Y:");
    lcd_gotoxy(13,2);
    lcd_printf("Z:");

    //lcd_gotoxy(0,3);
    //lcd_puts("No Errors Found");

    lcd_led(1);

    lcd_done = 1;

}

void lcd_updateVals( uint16_t temp_val, double acc_x_val, double acc_y_val, double acc_z_val, uint8_t distance_val, double prs_value )
{

    lcd_done = 0;

    /// Convert all values so they are ready if needed
    acc_conv_d = (acc_x_val - acc_0_g) / acc_scale; // Convert x value double to string
    dtostrf(acc_conv_d, 3, 1, acc_conv_out_x);
    acc_conv_d = (acc_y_val - acc_0_g) / acc_scale; // Convert y value double to string
    dtostrf(acc_conv_d, 3, 1, acc_conv_out_y);
    acc_conv_d = (acc_z_val - acc_0_g) / acc_scale; // Convert z value double to string
    dtostrf(acc_conv_d, 3, 1, acc_conv_out_z);
    prs_conv_d = PRS_CONV(prs_value);
    dtostrf(prs_conv_d, 3, 1, prs_conv_out);

    if (temp_val != raw_temperature_old)
    {
        lcd_gotoxy(LCD_TEMP);
        lcd_puts("     ");
        lcd_gotoxy(LCD_TEMP);
        lcd_printf("%u%cF", temp_val, DEG); // raw_temperature
    }

    if (acc_x_val != acc_x_old)
    {
        lcd_gotoxy(LCD_ACC_X);
        lcd_puts("    ");
        lcd_gotoxy(LCD_ACC_X);
        lcd_puts(acc_conv_out_x);
        lcd_puts("G");
    }

    if (acc_y_val != acc_y_old)
    {
        lcd_gotoxy(LCD_ACC_Y);
        lcd_puts("    ");
        lcd_gotoxy(LCD_ACC_Y);
        lcd_puts(acc_conv_out_y);
        lcd_puts("G");
    }

    if (acc_z_val != acc_z_old)
    {
        lcd_gotoxy(LCD_ACC_Z);
        lcd_puts("    ");
        lcd_gotoxy(LCD_ACC_Z);
        lcd_puts(acc_conv_out_z);
        lcd_puts("G");
    }

    if (distance_val != distance_old)
    {
        lcd_gotoxy(LCD_US);
        lcd_puts("     ");
        lcd_gotoxy(LCD_US);
        lcd_printf("%uin", distance_val);
    }

    if (prs_value != prs_val_old)
    {
        lcd_gotoxy(LCD_PRS);
        lcd_puts("    ");
        lcd_gotoxy(LCD_PRS);
        lcd_puts(prs_conv_out); // prs_val
        lcd_puts("PSI");
    }

    raw_temperature_old = temp_val;
    acc_x_old = acc_x_val;
    acc_y_old = acc_y_val;
    acc_z_old = acc_z_val;
    distance_old = distance_val;
    prs_val_old = prs_value;


    lcd_done = 1;
}

uint8_t lcd_ready( void )
{
    return (update_lcd);
}

void lcd_resetStatus( void )
{
    update_lcd = 0;
}

uint8_t ds18b20_dataReady( void )
{
    return (update_temp);
}

uint16_t ds18b20_print(uint8_t deg)
{

    raw_temperature = ds18b20_gettemp();

    if (deg == F)
    {
        raw_temperature = raw_temperature * 1.8 + 32;
    }

    else if (deg == C)
    {
        raw_temperature = raw_temperature;
    }

    else
    {
        //TODO: Output the text below to LCD
        //TODO: printf("Specify a Temperature Unit\n");
    }

    return raw_temperature; // Not being used for anything now
}

void ds18b20_resetStatus( void )
{
    update_temp = 0;
}

uint8_t hcsr04_isSensing( void )
{
    return (sensing);
}

void hcsr04_resetSensingComp ( void )
{
    sensing_complete = 0;
}

uint8_t hcsr04_getDist(uint8_t incm)
{

    uint8_t temp = 0;
    uint32_t time = t1_totalTimeUs();

    if ( time > 38000 )
    {
        return ( 1 );
    }

    if (incm == 0)
    {
        temp = time / US_IN_MOD;
    }
    else if (incm == 1)
    {
        temp = time / US_CM_MOD;
    }
    else
    {
        return ( 1 );
    }
    return (temp);
}

uint8_t mpx_dataReady ( void )
{
    return (prs_complete);
}

void mpx_resetStatus ( void )
{
    prs_complete = 0;
}

double mpx_getValue(void)
{

    double result;
    result = averageMPXSamples(adc_mpx_samples);

    return (result);
}

uint8_t adxl_dataReady( void )
{
    return (acc_complete);
}

void adxl_resetStatus ( void )
{
    acc_complete = 0;
}

struct adxl_g_values adxl_getADXLGValues( void )
{

    struct adxl_g_values adxl_g_values;

    adxl_g_values = averageADXLSamples(adc_adxl_samples);
    return (adxl_g_values);

}

struct adxl_g_values averageADXLSamples(struct adxl_g_values *sample_array)
{

    double sum_x, sum_y, sum_z;

    struct adxl_g_values adxl_averages;

    for(adxl_g_ptr = &sample_array[NUM_SMPL - 1]; adxl_g_ptr >= &sample_array[0]; adxl_g_ptr--)
    {
        sum_x += adxl_g_ptr->x;
        sum_y += adxl_g_ptr->y;
        sum_z += adxl_g_ptr->z;

    }

    adxl_averages.x = sum_x / NUM_SMPL;
    adxl_averages.y = sum_y / NUM_SMPL;
    adxl_averages.z = sum_z / NUM_SMPL;

    return (adxl_averages);

}

double averageMPXSamples (uint16_t *sample_array)
{
    double sum;
    double average;

    for(mpx_val_ptr = &sample_array[NUM_SMPL-1]; mpx_val_ptr >= &sample_array[0]; mpx_val_ptr--)
    {
        sum += *mpx_val_ptr;
    }

    average = sum / NUM_SMPL;

    return (average);
}

ISR(ADC_vect)
{

    adc_raw = ADC; //Take the ADC reading ASAP (16 bit value stored in ADC


    if (first_read >= 1)
    {

        switch (ADMUX)
        {

        case 0x40: // ADC0
            // Read value and set next read to pin 1
            if (adc_sample < NUM_SMPL)
            {
                adxl_g_ptr = &adc_adxl_samples[adc_sample];
                adxl_g_ptr->x = adc_raw;
                adc_sample++;
                break;
            }
            else
            {
                ADMUX = (ADMUX_MSK & ADMUX) | ACC_Y_PIN; // Clear MUX0:4 and set MUX0 to 1
                adc_sample = 0;
                first_read = 0; // Reset if we took a reading
                break;
            }
        case 0x41: // ADC1
            // Read value and set next read to pin 2
            if (adc_sample < NUM_SMPL)
            {
                adxl_g_ptr = &adc_adxl_samples[adc_sample];
                adxl_g_ptr->y = adc_raw;
                adc_sample++;
                break;
            }
            else
            {
                ADMUX = (ADMUX_MSK & ADMUX) | ACC_Z_PIN; // Clear MUX0:4 and set MUX0 to 1
                adc_sample = 0;
                first_read = 0; // Reset if we took a reading
                break;
            }
        case 0x42: // ADC2
            if (adc_sample < NUM_SMPL)
            {
                adxl_g_ptr = &adc_adxl_samples[adc_sample];
                adxl_g_ptr->z = adc_raw;
                adc_sample++;
                break;
            }
            else
            {
                ADMUX = (ADMUX_MSK & ADMUX) | PRS_PIN; // Clear MUX0:4 and set MUX0 to 1
                adc_sample = 0;
                first_read = 0; // Reset if we took a reading
                acc_complete = 1;
                break;
            }
        case 0x43: // ADC3
            // Read value and set next read to pin 0
            if (adc_sample < NUM_SMPL)
            {
                mpx_val_ptr = &adc_mpx_samples[adc_sample];
                *mpx_val_ptr = adc_raw;
                adc_sample++;
                break;
            }
            else
            {
                ADMUX = (ADMUX_MSK & ADMUX) | ACC_X_PIN;
                adc_sample = 0;
                first_read = 0;
                prs_complete = 1;
                break;
            }
        }
        first_read = 0;
    }
    else
    {
        first_read++;
    }
}

ISR(INT0_vect)
{
    if ( sensing )
    {
        t1_stop_val = TCNT1; // See how much time has elapsed on logic change
        sensing = 0;
        TCCR1B &= ~(1 << CS10); // Disable timer
        TCNT1 = 0; // Reset timer count

        sensing_complete = 1; // Let main know it can process the US values
    }

    else
    {
        TCCR1B |= (1 << CS10); // Start sensing on the any logical change
        TCNT1 = 0;
        sensing = 1;
        t1_stop_val = 0;
        t1_ovf_count = 0;
    }
}

ISR(TIMER0_OVF_vect)
{

    t0_ovf_count++;

    if (!(t0_ovf_count % 20) && lcd_done)
    {
        update_lcd = 1; // Update the LCD every other loop
    }

    if ( t0_ovf_count >= 61 )   // 15625 counts (61 overflows) == 1 seconds @ prescale of 1024
    {

        FLIPBIT(PORTB, LED_PIN);

        update_temp = 1; //Get temp next loop

        t0_ovf_count = 0;
    }
}

ISR(TIMER1_OVF_vect)
{

    t1_ovf_count++; // Increment our overflow count

    // Restart sensor if >41mS has passed 4.096mS/overflow
    if (t1_ovf_count >= 10)
    {
        //TODO: printf("Frozen!\n");
        sensing = 0;
        TCCR1B &= ~(1 << CS10); // Disable timer
        //TODO: printf("Restarting Sensor\n");
    }
}

ISR(TIMER2_COMPA_vect)
{

    if (t2_dir == timer_up)
    {
        if (OCR2A < 255)
        {
            OCR2A = OCR2A + t2_dist_coef;
        }
        else
        {
            t2_dir = timer_down;
        }
    }

    else if (t2_dir == timer_down)
    {
        if (OCR2A > 0)
        {
            OCR2A = OCR2A - t2_dist_coef;
        }
        else
        {
            t2_dir = timer_up;
        }
    }

}

ISR(BADISR_vect)
{
    _delay_ms(200);
    //TODO: printf("Bad ISR Triggered\n");
}
